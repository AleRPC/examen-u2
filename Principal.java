//Examen práctico U2

    //---*PARTE 2*---

//Punto 1: crear una clase pública y de nombre "Principal"
public class Principal {
    //Punto 2: crear el punto de entrada main
    public static void main(String[] args){
        //Punto 3: crear una instancia de tipo "PruebaExamen" de nombre "pe"
        PruebaExamen pe = new PruebaExamen();

        //Punto 4: Asigna una expresión lambda a la instancia "pe", en la cual se imprima la suma
        //de dos números
        //system.out.println("==> El resultado de la suma de los tres números es: "+ PruebaExamen.operacionPrueba(5,6));

        //Paso 5: invocar "operacionPrueba" e imprimir el resultado
        //de la suma de dos números enteros

        //Paso 6: invocar "mensajeHola"
        pe.mensajeHola();
        //paso 7: invocar "mensajeHola" y enviar cualquier String

        //paso 8: Asignar una expresión lambda a la instancia "pe", en la cual se 
        //imprima la suma del primer argumento con la multiplicación del segundo argumento por el tercer argumento

        /*Principal.engine((int x,int y) -> x + y);
        Principal.engine((int x,int y) -> x * y);
        Principal.engine((int x,int y) -> x / y);
        Principal.engine((int x,int y) -> x - y);
        Principal.engine((long x,long y) -> x % y);*/
    }
    

    }