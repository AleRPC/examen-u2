//Examen práctico U2
    //---*PARTE 1*---
//Punto 1: Crear una interfaz pública de nombre "PruebaExamen"
public interface PruebaExamen{
    
    //Punto 2: Crear un método por defecto y público de nombre "mensajeHola"
    public default void mensajeHola() {   
        //Punto 3: Imprimir un mensaje  
        System.out.println("Hola, mi nombre es: Alejandra Pérez"); 
    }

    //Punto 4: crear un método por defecto y público de nombre "mensajeHola", recibir como parámetros
    //un string e imprimir ese String ""La cadena es:"+cadena"
    public default void mensajeHola(String cadena){
        System.out.println("La cadena es: " + cadena);
    }

    //Punto 5: En esta misma interfaz crear un método público y estático
    //de nombre "operaciónPrueba", retorna un tipo de dato entero y va a recibir
    //dos parámetros detipo entero
    public static int operacionPrueba(int a, int b){
    //Punto 6: retornar la suma de estos dos parámetros
        return (a+b);
    }

    //Punto 7: Crear un método abstracto de nombre "operacionPrueba", recibe tres parámetros de tipo entero
    public int operacionPrueba(int c, int d, int e);
}
